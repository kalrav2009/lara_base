<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});

Route::get('admin/login', ['middleware' => 'guest', function () {
    return view('auth.login');
}])->name('admin.login');

Auth::routes();

/* Front Routes */
Route::group(['namespace' => 'Front', 'middleware' => ['auth']], function () {
	Route::get('/home', 'PageController@index')->name('front.home');
});
/* ************************* */
Route::get('/admin', function () {
	return Redirect::route('admin.login');
});

Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => ['auth', 'role:admin']], function () {

	Route::get('/home', 'HomeController@index')->name('home');

	/* Users */
		Route::get('/users/data', 'UserController@data')->name('users.data');
		Route::match(['get', 'post'], '/users/userdocument', 'UserController@userdocument')->name('users.userdocument');
		Route::post('/users/download', 'UserController@getDownload')->name('users.getDownload');
		Route::match(['get', 'patch'], '/users/{user}/change_pass', 'UserController@changePass')->name('users.change_pass');
		Route::resource('users', 'UserController');
	/* ********************** */

	/* Permissions */
		Route::get('/permissions/data', 'PermissionController@data')->name('permissions.data');
		Route::get('/permissions/get_actions/{controller}', 'PermissionController@getControllerActions')->name('permissions.get_actions');
		Route::resource('permissions', 'PermissionController', ['except' => ['show']]);
	/* ********************** */

	/* Roles */
		Route::get('/roles/data', 'RoleController@data')->name('roles.data');
		Route::resource('roles', 'RoleController');
	/* ********************** */

	/* Options */
		Route::get('/options/data', 'OptionController@data')->name('options.data');
		Route::resource('options', 'OptionController', ['except' => ['show', 'destroy']]);
	/* ********************** */
});
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $guarded = [
        'id'
    ];
    
    public static function getData($type){
    	 $option = Option::where('type', $type)->get()->pluck('title', 'id')->toArray();; 
    	 return $option;
    }
}

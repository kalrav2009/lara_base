<?php 
namespace App;

use Config;
use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
	protected $guarded = [ 'id' ];
	
	public function permissions()
    {
        return $this->belongsToMany('App\Permission', 'permission_role');
    }
}
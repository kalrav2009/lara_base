<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use File;
use Datatables;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use App\Role;
use App\Permission;
use App\Option;
use Config;
use Response;


class UserController extends Controller
{
    public function index()
    {
        return view('users.index');
    }

    public function data()
    {
    	$users = User::get();
        return Datatables::of($users)
        ->rawColumns(['actions','count'])
        ->editColumn('status', function ($users) {
            if($users->status == 1){
                return 'Enabled';
            } else {
                return 'Disabled';
            }
        })
        ->addColumn('actions', function ($users) {
            $view = '<a href="'.route('users.show', $users->id).'" class="btn btn-sm bg-blue btn-flat"><i class="fa fa-eye"></i></a>';
            $edit = '<a href="'.route('users.edit', $users->id).'" class="btn btn-sm bg-green btn-flat"><i class="fa fa-pencil"></i></a>';
            $change_pass = '<a href="'.route('users.change_pass', $users->id).'" class="btn btn-sm bg-purple btn-flat"><i class="fa fa-lock"></i></a>';
            $delete = '<a href="'.route('users.destroy', $users->id).'" class="btn btn-sm bg-red btn-flat btn-delete"><i class="fa fa-trash"></i></a>';
            $action = $view.' '.$edit.' '.$change_pass.' '.$delete;
            return $action;
        })
        ->make(true);
    }

    public function create()
    {
    	$roles = Role::orderBy('display_name', 'asc')->get()->pluck('display_name', 'id');
        return view('users.create', compact('roles'));
    }

    public function store(Request $request)
    {
       $rules = [
			'name'          => 'required',
			'profile_image' => 'nullable|image|mimes:jpg,jpeg,png,gif,bmp|max:1000',
			'email'         => 'email|required|unique:users|max:255',
            'password'      => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
	        'role'        => 'required',
		];

		$this->validate($request, $rules);

		$file_location = null;
		if ($request->hasFile('profile_image')) {
			$image_name = sha1(time());
			$extension = $request->file('profile_image')->extension();
		    $request->file('profile_image')->move('uploads/user-images', $image_name.'.'.$extension);
			$file_location = 'uploads/user-images/'.$image_name.'.'.$extension;
		}

		$user = User::create([
			'name'  		=> $request->get('name'),
			'email' 		=> $request->get('email'),
			'profile_image' => (!is_null($file_location))?$file_location:'',
			'password' 		=> Hash::make($request->get('password')),
			'status'        => $request->get('status')
		]);

		if ($user) {
			if(!empty($request->get('role'))){
				$user->attachRole($request->get('role'));
			}
			$data = ['response' => 1, 'msg' => 'user added successfully', 'redirect' => route('users.show', $user->id)];
		} else {
			$data = ['response' => 2, 'msg' => 'failed to add user', 'redirect' => route('users.index')];
		}
		return response()->json($data);
    }

    public function show($id)
    {
		$user = User::with('roles')->findOrFail($id);
		return view('users.show', compact('user'));
    }

    public function edit($id)
    {
        $user = User::with('roles')->findOrFail($id);
        $roles = Role::orderBy('display_name', 'asc')->get()->pluck('display_name', 'id');
		return view('users.edit', compact('user', 'roles'));
    }

    public function update($id, Request $request)
    {
        $user = User::findOrFail($id);

	    $rules = [
			'name' => 'required',
			'email' => ($user->email == $request->get('email'))?'email|required|max:255':'email|required|unique:users|max:255', 
			'profile_image' => 'nullable|image|mimes:jpg,jpeg,png,gif,bmp|max:1000',
	        'role' => 'required',
		];
		$this->validate($request, $rules);

		$file_location = null;
		if ($request->hasFile('profile_image')) {
			$image_name = sha1(time());
			$extension = $request->file('profile_image')->extension();
		    $request->file('profile_image')->move('uploads/user-images', $image_name.'.'.$extension);
			$file_location = 'uploads/user-images/'.$image_name.'.'.$extension;
			if(File::exists($user->profile_image)){
				File::delete($user->profile_image);
			}
		}

		$input = [
			'name'  => $request->get('name'),
			'email' => $request->get('email'),
			'status'        => $request->get('status')
		];
		if(!is_null($file_location)){
			$input['profile_image'] = $file_location;	
		}

		if ($user->fill($input)->save()) {
			if(!empty($request->get('role'))){
				$user->roles()->detach();
				$user->attachRole($request->get('role'));
			}
			$data = ['response' => 1, 'msg' => 'user updated successfully', 'redirect' => route('users.show', $user->id)];
		} else {
			$data = ['response' => 2, 'msg' => 'failed to update user', 'redirect' => route('users.index')];
		}
		return response()->json($data);
    }

    public function destroy($id)
    {
    	$user = User::findOrFail($id);
    	if($id != Auth::user()->id){
    		$user->roles()->detach();
    		if($user->delete()){
	    		$data = ['response' => 1, 'msg' => 'User deleted successfully'];
	    	} else {
	    		$data = ['response' => 2, 'msg' => 'Failed to delete User'];
	    	}	
    	} else {
			$data = ['response' => 2, 'msg' => 'You Can\'t delete Your Self'];
    	}
        return response()->json($data);
    }

    public function changePass($id='', Request $request)
	{
		$user = User::findOrFail($id);
		if($request->isMethod('patch')){
			$rules = [
    			'password' => 'required|min:6|confirmed',
		        'password_confirmation' => 'required|min:6'
    		];
    		$this->validate($request, $rules);

    		$input['password'] = Hash::make($request->get('password'));

    		if ($user->fill($input)->save()) {
				$data = ['response' => 1, 'msg' => 'password changed successfully', 'redirect' => route('users.index')];
			} else {
				$data = ['response' => 2, 'msg' => 'failed to update password', 'redirect' => route('users.index')];
			}

			return response()->json($data);
		}
		return view('users.change_pass', compact('user'));
	}
}

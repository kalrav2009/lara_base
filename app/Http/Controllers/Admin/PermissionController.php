<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Permission;
use Datatables;
use Carbon\Carbon;

class PermissionController extends Controller
{
    public function index()
    {
        return view('permissions.index');
    }

	public function data()
    {
        $permissions = Permission::select(['id', 'name', 'display_name', 'permission_group', 'description', 'created_at']);
        return Datatables::of($permissions)
        ->rawColumns(['actions'])
        ->editColumn('created_at', function ($permissions) {
            return $permissions->created_at ? with(new Carbon($permissions->created_at))->format('d/m/Y') : '';
        })
        ->addColumn('actions', function (Permission $permissions) {
            $edit = '<a href="'.route('permissions.edit', $permissions->id).'" class="btn btn-sm bg-green btn-flat"><i class="fa fa-pencil"></i></a>';
            $delete = '<a href="'.route('permissions.destroy', $permissions->id).'" class="btn btn-sm bg-red btn-flat btn-delete"><i class="fa fa-trash"></i></a>';
            $action = $edit.' '.$delete;
            return $action;
        })
        ->make(true);
    }

    public function create()
    {
    	$permission_groups = $this->getControllers();
        return view('permissions.create', compact('permission_groups'));
    }

    public function store(Request $request)
    {
		$this->validate($request, [
            'name' => 'required|unique:permissions',
            'display_name' => 'required|unique:permissions',
            'permission_group' => 'required',
        ]);

		$permission = Permission::create([
			'name'  		=> $request->get('name'),
			'display_name' 	=> $request->get('display_name'),
			'permission_group' => $request->get('permission_group'),
			'description' 	=> $request->get('description'),
		]);

		if ($permission) {
			$data = ['response' => 1, 'msg' => 'permission created successfully', 'redirect' => route('permissions.index')];
		} else {
			$data = ['response' => 2, 'msg' => 'failed to create permission', 'redirect' => route('permissions.index')];
		}
		return response()->json($data);
    }

    public function edit($id)
    {
        $permission = Permission::findOrFail($id);
        $permission_groups = $this->getControllers();

		return view('permissions.edit', compact('permission', 'permission_groups'));
    }

    public function update($id, Request $request)
    {
        $permission = Permission::findOrFail($id);

		$this->validate($request, [
            'name' => ($permission->name == $request->get('name'))?'required':'required|unique:permissions',
            'display_name' => ($permission->display_name == $request->get('display_name'))?'required':'required|unique:permissions',
            'permission_group' => 'required',
        ]);

		$input = [
			'name'  		=> $request->get('name'),
			'display_name' 	=> $request->get('display_name'),
            'permission_group' => $request->get('permission_group'),
			'description' 	=> $request->get('description'),
		];

		if ($permission->fill($input)->save()) {
			$data = ['response' => 1, 'msg' => 'permission updated successfully', 'redirect' => route('permissions.index')];
		} else {
			$data = ['response' => 2, 'msg' => 'failed to update permission', 'redirect' => route('permissions.index')];
		}
		return response()->json($data);
    }

    public function destroy($id)
    {
        $permission = Permission::findOrFail($id);
		if($permission->delete()){
    		$data = ['response' => 1, 'msg' => 'Permission deleted successfully'];
    	} else {
    		$data = ['response' => 2, 'msg' => 'Failed to delete Permission'];
    	}
        return response()->json($data);
    }
}

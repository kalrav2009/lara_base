<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Config;
use App\Option;
use Datatables;
use Carbon\Carbon;

class OptionController extends Controller
{
    public function index()
    {
        return view('options.index');
    }

    public function data()
    {
        $options = Option::select(['id', 'type','title', 'status']);
        return Datatables::of($options)
        ->rawColumns(['actions'])
        ->editColumn('type', function ($options) {
            return Config::get('constants.options_categories')[$options->type];
        })
        ->editColumn('status', function ($options) {
            if($options->status == 1){
                return 'Enabled';
            } else {
                return 'Disabled';
            }
        })
        ->addColumn('actions', function (Option $options) {
            $action = '<a href="'.route('options.edit', $options->id).'" class="btn btn-sm bg-green btn-flat"><i class="fa fa-pencil"></i></a>';
            return $action;
        })
        ->make(true);
    }

    public function create()
    {
        return view('options.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'type' => 'required',
            'title' => 'required',
        ]);

        $option = Option::create($request->all());

        if ($option) {
            $data = ['response' => 1, 'msg' => 'Option Created Successfully', 'redirect' => route('options.index')];
        } else {
            $data = ['response' => 2, 'msg' => 'Failed To Create Option', 'redirect' => route('options.index')];
        }
        return response()->json($data);
    }

    public function edit($id)
    {
        $option = Option::findOrFail($id);
        return view('options.edit', compact('option'));
    }

    public function update(Request $request, $id)
    {
        $option = Option::findOrFail($id);

        $this->validate($request, [
            'title' => 'required',
        ]);

        if ($option->fill($request->all())->save()) {
            $data = ['response' => 1, 'msg' => 'Option Updated Successfully', 'redirect' => route('options.index')];
        } else {
            $data = ['response' => 2, 'msg' => 'Failed To Update Option', 'redirect' => route('options.index')];
        }
        return response()->json($data);
    }
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Role;
use Datatables;
use Carbon\Carbon;
use App\Permission;

class RoleController extends Controller
{
    public function index()
    {
        return view('roles.index');
    }

	public function data()
    {
        $roles = Role::select(['id', 'name', 'display_name', 'description', 'created_at']);
        return Datatables::of($roles)
        ->rawColumns(['actions'])
        ->editColumn('created_at', function ($roles) {
            return $roles->created_at ? with(new Carbon($roles->created_at))->format('d/m/Y') : '';
        })
        ->addColumn('actions', function (Role $roles) {
            $edit = '<a href="'.route('roles.edit', $roles->id).'" class="btn bg-green btn-flat"><i class="fa fa-pencil"></i></a>';
            $delete = '<a href="'.route('roles.destroy', $roles->id).'" class="btn bg-red btn-flat btn-delete"><i class="fa fa-trash"></i></a>';
            $action = $edit.' '.$delete;
            return $action;
        })
        ->make(true);
    }

    public function create()
    {
        $permissions = Permission::orderBy('permission_group', 'asc')->orderBy('display_name', 'asc')->get();
        $permission_options = [];
        foreach ($permissions as $permission) {
            $permission_options[$permission['permission_group']][$permission['id']] = $permission['display_name'];
        }
        return view('roles.create', compact('permission_options'));
    }

    public function store(Request $request)
    {
        $rules = [
			'name' => 'required|unique:roles',
			'display_name' => 'required|unique:roles',
		];
		$this->validate($request, $rules);

		$role = Role::create([
			'name'  		=> $request->get('name'),
			'display_name' 	=> $request->get('display_name'),
			'description' 	=> $request->get('description'),
		]);

        if(!empty($request->get('permissions'))){
            $role->attachPermissions($request->get('permissions'));
        }

		if ($role) {
			$data = ['response' => 1, 'msg' => 'role created successfully', 'redirect' => route('roles.index')];
		} else {
			$data = ['response' => 2, 'msg' => 'failed to create role', 'redirect' => route('roles.index')];
		}
		return response()->json($data);
    }

    public function edit($id)
    {
        $role = Role::with('permissions')->findOrFail($id);
        $permissions = Permission::orderBy('permission_group', 'asc')->orderBy('display_name', 'asc')->get();
        $permission_options = [];
        foreach ($permissions as $permission) {
            if($role->permissions->contains($permission['id'])){
                $permission_options[$permission['permission_group']][$permission['id']] = ['ticked' => 'checked', 'display_name' => $permission['display_name']];
            } else {
                $permission_options[$permission['permission_group']][$permission['id']] = ['ticked' => '', 'display_name' => $permission['display_name']];
            }
        }
		return view('roles.edit', compact('role', 'permission_options'));
    }

    public function update($id, Request $request)
    {
        $role = Role::findOrFail($id);

	    $rules = [
			'name' => ($role->name == $request->get('name'))?'required':'required|unique:roles',
			'display_name' => ($role->display_name == $request->get('display_name'))?'required':'required|unique:roles',
		];
		$this->validate($request, $rules);

		$input = [
			'name'  		=> $request->get('name'),
			'display_name' 	=> $request->get('display_name'),
			'description' 	=> $request->get('description'),
		];

        $role->permissions()->detach();
        if(!empty($request->get('permissions'))){
            $role->attachPermissions($request->get('permissions'));
        }
        
		if ($role->fill($input)->save()) {
			$data = ['response' => 1, 'msg' => 'role updated successfully', 'redirect' => route('roles.index')];
		} else {
			$data = ['response' => 2, 'msg' => 'failed to update role', 'redirect' => route('roles.index')];
		}
		return response()->json($data);
    }

    public function destroy($id)
    {
        $role = Role::findOrFail($id);
        $role->permissions()->detach();
		if($role->delete()){
    		$data = ['response' => 1, 'msg' => 'Role deleted successfully'];
    	} else {
    		$data = ['response' => 2, 'msg' => 'Failed to delete Role'];
    	}
        return response()->json($data);
    }
}

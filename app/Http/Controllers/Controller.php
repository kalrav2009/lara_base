<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Route;
use Config;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getControllers()
    {
    	$controllers = [];
		foreach (Route::getRoutes()->getRoutes() as $route)
		{
		    $action = $route->getAction();

		    if (array_key_exists('controller', $action))
		    {
		    	$controller_action_path = explode('@', $action['controller']);

		        $controller = explode('\\', $controller_action_path[0]);
		        $controllers[] = [str_replace('Controller', '', end($controller))];
		    }
		}
		$only_controllers = [];
		foreach ($controllers as $controller) {
			$only_controllers[$controller[0]] = $controller[0];
		}
		$only_controllers = $only_controllers + Config::get('constants.customPermissionGroups');
		return $only_controllers;
    }
}

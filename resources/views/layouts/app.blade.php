<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>@yield('title') | {{ config('app.name') }}</title>
  <!-- Scripts -->
  <script>
      window.Laravel = {!! json_encode([
          'csrfToken' => csrf_token(),
      ]) !!};
  </script>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{{asset('/assets/plugins/bootstrap/css/bootstrap.min.css')}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('/assets/plugins/fontawesome/css/font-awesome.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{asset('/assets/plugins/ionicons/css/ionicons.min.css')}}">
  @stack('styles')
  <!-- iCheck -->
  <link rel="stylesheet" href="{{asset('/assets/plugins/iCheck/flat/blue.css')}}">
  <!-- Date Picker -->
  <link rel="stylesheet" href="{{asset('/assets/plugins/datepicker/datepicker3.css')}}">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="{{asset('/assets/plugins/timepicker/bootstrap-timepicker.min.css')}}">
  <!-- Sweet Alert -->
  <link rel="stylesheet" href="{{asset('/assets/plugins/sweetalert/sweetalert.css')}}">
  <!-- Select2 -->
  <link rel="stylesheet" href="{{asset('/assets/plugins/select2/select2.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('/assets/css/AdminLTE.min.css')}}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{asset('/assets/plugins/iCheck/square/blue.css')}}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{asset('/assets/css/skins/_all-skins.min.css')}}">
  <link rel="stylesheet" href="{{asset('/assets/css/custom.css')}}">
  
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-purple sidebar-mini">
<div class="wrapper">
  @include('partials.header')
  @include('partials.sidebar')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        @yield('title')
        <small>@yield('sub_title')</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">@yield('title')</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      @yield('content')
    </section>
    <!-- /.content -->
  </div>
  @include('partials.footer')
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="{{asset('/assets/plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{asset('/assets/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="{{asset('/assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
<!-- datepicker -->
<script src="{{asset('/assets/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<!-- bootstrap time picker -->
<script src="{{asset('/assets/plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>
<!-- Slimscroll -->
<script src="{{asset('/assets/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('/assets/plugins/fastclick/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('/assets/js/app.min.js')}}"></script>
<!-- Sweet Alert -->
<script src="{{asset('/assets/plugins/sweetalert/sweetalert.min.js')}}"></script>
<!-- Select2 -->
<script src="{{asset('/assets/plugins/select2/select2.full.min.js')}}"></script>
<!-- jQuery Form -->
<script src="{{asset('/assets/plugins/jquery-form/jquery.form.js')}}"></script>
<!-- iCheck -->
<script src="{{asset('/assets/plugins/iCheck/icheck.min.js')}}"></script>
<!-- input Mask -->
<script src="{{asset('/assets/plugins/input-mask/jquery.inputmask.js')}}"></script>
<script src="{{asset('/assets/plugins/input-mask/jquery.inputmask.numeric.extensions.js')}}"></script>

<!-- Custom Js -->
<script src="{{asset('/assets/js/custom.js')}}"></script>
@stack('scripts')
</body>
</html>

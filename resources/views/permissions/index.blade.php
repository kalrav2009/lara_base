@extends('layouts.app')

@section('title', 'Permissions')
@section('sub_title', 'Permissions Listing')

@push('styles')
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('/assets/plugins/datatables/dataTables.bootstrap.css')}}">
@endpush

@section('content')
    <div class="row">
        <div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Permissions List</h3>
					<a href="{{ route('permissions.create') }}" class="btn bg-purple btn-flat pull-right"><i class="fa fa-plus"></i> Create New</a>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<table class="table table-bordered table-striped" id="permissions-table">
						<thead>
							<tr>
								<th>Group</th>
				                <th>Name</th>
				                <th>Action</th>
				                <th>Description</th>
				                <th width="85">Actions</th>
							</tr>
						</thead>
					</table>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@stop

@push('scripts')
<!-- DataTables -->
<script src="{{asset('/assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('/assets/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript">
	var dTable = '';
	jQuery(function() {
	    dTable = jQuery('#permissions-table').DataTable({
	        processing: true,
	        serverSide: true,
	        ajax: '{{ route('permissions.data') }}',
	        columns: [
	        	{ data: 'permission_group', name: 'permission_group'},
		        { data: 'display_name', name: 'display_name' },
	            { data: 'name', name: 'name' },
	            { data: 'description', name: 'description' },
	            { data: 'actions', name: 'actions', orderable: false, searchable: false }
	        ]
	    });
	});

	function loadDataTable() {
	    dTable.ajax.reload( null, false );
	}
</script>
@endpush
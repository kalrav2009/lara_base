@extends('layouts.app')

@section('title', 'Permissions')
@section('sub_title', 'Edit Permission')

@section('content')
	<div class="row">
		<!-- left column -->
        <div class="col-md-6">
          	<!-- general form elements -->
          	<div class="box box-primary">
            	<div class="box-header with-border">
              		<h3 class="box-title">Edit Permission</h3>
            	</div>
            	<!-- /.box-header -->
            	<!-- form start -->
            	{!! Form::open(['method' => 'PATCH', 'route' => ['permissions.update', $permission->id]]) !!}
	              	<div class="box-body">
	              		<div class="form-group">
		                	{!! Form::label('permission_group', 'Permission Group') !!}
		                	{!! Form::select('permission_group', $permission_groups, $permission->permission_group, ['placeholder' => 'Select Permission Group', 'class' => 'form-control']); !!}
		                </div>
						<div class="form-group">
							{!! Form::label('name', 'Name') !!}
							{!! Form::text('name', $permission->name, ['placeholder' => 'Enter Name', 'class' => 'form-control']); !!}
						</div>
						<div class="form-group">
		                	{!! Form::label('display_name', 'Display Name') !!}
		                	{!! Form::text('display_name', $permission->display_name, ['placeholder' => 'Enter Display Name', 'class' => 'form-control']); !!}
		                </div>
						<div class="form-group">
		                	{!! Form::label('description', 'Description') !!}
		                	{!! Form::textarea('description', $permission->description, ['placeholder' => 'Enter Description', 'class' => 'form-control', 'rows' => '3']); !!}
		                </div>
	              	</div>
	              	<!-- /.box-body -->

	              	<div class="box-footer text-center">
		              	<button type="submit" class="btn bg-blue btn-flat ajax-submit">Save</button>
	              		<a href="{{ route('permissions.index') }}" class="btn bg-red btn-flat">Cancel</a>
	              	</div>
            	{!! Form::close() !!}
          	</div>
          	<!-- /.box -->
        </div>
        <!--/.col (left) -->
	</div>
@endsection
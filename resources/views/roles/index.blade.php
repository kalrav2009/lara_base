@extends('layouts.app')

@section('title', 'Roles')
@section('sub_title', 'Roles Listing')

@push('styles')
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('/assets/plugins/datatables/dataTables.bootstrap.css')}}">
@endpush

@section('content')
    <div class="row">
        <div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Roles List</h3>
					<a href="{{ route('roles.create') }}" class="btn bg-purple btn-flat pull-right"><i class="fa fa-plus"></i> Create New</a>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<table class="table table-bordered table-striped" id="roles-table">
						<thead>
							<tr>
								<th>Id</th>
				                <th>Name</th>
				                <th>Display Name</th>
				                <th>Description</th>
				                <th>Actions</th>
							</tr>
						</thead>
					</table>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@stop

@push('scripts')
<!-- DataTables -->
<script src="{{asset('/assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('/assets/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript">
	var dTable = '';
	jQuery(function() {
	    dTable = jQuery('#roles-table').DataTable({
	        processing: true,
	        serverSide: true,
	        ajax: '{{ route('roles.data') }}',
	        columns: [
	            { data: 'id', name: 'id' },
	            { data: 'name', name: 'name' },
	            { data: 'display_name', name: 'display_name' },
	            { data: 'description', name: 'description' },
	            { data: 'actions', name: 'actions', orderable: false, searchable: false }
	        ]
	    });
	});

	function loadDataTable() {
	    dTable.ajax.reload( null, false );
	}
</script>
@endpush
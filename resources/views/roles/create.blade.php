@extends('layouts.app')

@section('title', 'Roles')
@section('sub_title', 'Create Role')

@section('content')
	<div class="row">
		<!-- left column -->
        <div class="col-xs-12">
          	<!-- general form elements -->
          	<div class="box box-primary">
            	<div class="box-header with-border">
              		<h3 class="box-title">Create Role</h3>
            	</div>
            	<!-- /.box-header -->
            	<!-- form start -->
            	{!! Form::open(['route' => ['roles.store']]) !!}
	              	<div class="box-body">
	              		<div class="col-md-6">
	              			<div class="form-group">
			                	{!! Form::label('name', 'Name') !!}
			                	{!! Form::text('name', null, ['placeholder' => 'Enter Name', 'class' => 'form-control']); !!}
			                </div>
			                <div class="form-group">
			                	{!! Form::label('display_name', 'Display Name') !!}
			                	{!! Form::text('display_name', null, ['placeholder' => 'Enter Display Name', 'class' => 'form-control']); !!}
			                </div>
	              		</div>
	              		<div class="col-md-6">
	              			<div class="form-group">
			                	{!! Form::label('description', 'Description') !!}
			                	{!! Form::textarea('description', null, ['placeholder' => 'Enter Description', 'class' => 'form-control', 'rows' => '5']); !!}
			                </div>
              			</div>
	              		<div class="clearfix"></div>
		                <h4>Permissions</h4>
		                <hr>
		                @foreach($permission_options AS $permission_group => $permission_option)
			                <div class="row">
			                	<div class="col-sm-2 text-right">
			                		<label>{{ $permission_group }}</label>
			                	</div>
			                	<div class="col-sm-10">
			                		@foreach (array_chunk($permission_option, 5, true) AS $column)
			                			<div class="row">
			                				<?php $i = 1; ?>
			                				@foreach ($column AS $permission_name => $permission_display_name)
			                					<div class="col-xs-2 {{ ($i == 1)?'col-xs-offset-1':'' }}">
						                			<label class="checkbox-inline"><input type="checkbox" name="permissions[{{ $permission_name }}]" value="{{ $permission_name }}"> {{ $permission_display_name }}</label>
					                			</div>
					                			<?php $i++; ?>
			                				@endforeach
			                			</div>
			                			<br>
			                		@endforeach
			                	</div>
			                </div>
			                <br>
		                @endforeach
	              	</div>
	              	<!-- /.box-body -->
	              	

	              	<div class="box-footer text-center">
		              	<button type="submit" class="btn bg-blue btn-flat ajax-submit">Save</button>
	              		<a href="{{ route('roles.index') }}" class="btn bg-red btn-flat">Cancel</a>
	              	</div>
            	{!! Form::close() !!}
          	</div>
          	<!-- /.box -->
        </div>
        <!--/.col (left) -->
	</div>
@endsection
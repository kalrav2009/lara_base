<!-- Modal -->
<div class="modal fade" id="empty_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Loading</h4>
            </div>
            <div class="modal-body" id="append_modal_form">
                
            </div>
        </div>
    </div>
</div>

<div class="modal_loader_template" style="display:none;">
    <div class="text-center loader">
        <i class="fa fa-circle-o-notch fa-spin fa-3x" style="color:#8e8e8e;" aria-hidden="true"></i>
    </div>    
</div>

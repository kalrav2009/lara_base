  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ (!empty(Auth::user()->profile_image))?url(Auth::user()->profile_image):url('/assets/img').'/default-user.png' }}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{ ucwords(Auth::user()->name) }}</p>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
        @ability('admin', 'create_user,list_user')
        <li class="treeview">
          <a href="#">
            <i class="fa fa-users"></i> <span>Users</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            @ability('admin', 'create_user')
              <li><a href="{{ route('users.index') }}"><i class="fa fa-list"></i> List Users</a></li>
            @endability
            @ability('admin', 'list_user')
              <li><a href="{{ route('users.create') }}"><i class="fa fa-plus"></i> Create User</a></li>
            @endability
          </ul>
        </li>
        @endability

        <li class="treeview">
          <a href="#">
            <i class="fa fa-database"></i> <span>Masters</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="treeview">
              <a href="#">
                <i class="fa fa-cubes"></i> <span>Options</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{ route('options.index') }}"><i class="fa fa-list"></i> List Option</a></li>
                <li><a href="{{ route('options.create') }}"><i class="fa fa-plus"></i> Create Option</a></li>
              </ul>
            </li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-cogs"></i> <span>Settings</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          @ability('admin', 'manage_roles_and_permissions')
          <ul class="treeview-menu">
            <li class="treeview">
              <a href="#">
                <i class="fa fa-user-secret"></i> <span>Roles</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{ route('roles.index') }}"><i class="fa fa-list"></i> List Roles</a></li>
                <li><a href="{{ route('roles.create') }}"><i class="fa fa-plus"></i> Create Role</a></li>
              </ul>
            </li>

            <li class="treeview">
              <a href="#">
                <i class="fa fa-lock"></i> <span>Permissions</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{ route('permissions.index') }}"><i class="fa fa-list"></i> List Permissions</a></li>
                <li><a href="{{ route('permissions.create') }}"><i class="fa fa-plus"></i> Create Permission</a></li>
              </ul>
            </li>
          </ul>
          @endability
        </li>

        <li class="header">ACTIONS</li>

        <li class="treeview">
          <a href="{{ url('/logout') }}"  onclick="event.preventDefault(); document.getElementById('logout-form-siderbar').submit();">
            <i class="fa fa-sign-out text-red"></i> <span>Logout</span>
            <span class="pull-right-container">
          </a>
          
          <form id="logout-form-siderbar" action="{{ url('/logout') }}" method="POST" style="display: none;">
             {{ csrf_field() }}
          </form>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
@extends('layouts.app')

@section('title', 'Users')
@section('sub_title', 'Change User Password')

@section('content')
	<div class="row">
		<!-- left column -->
        <div class="col-md-6">
          	<!-- general form elements -->
          	<div class="box box-primary">
            	<div class="box-header with-border">
              		<h3 class="box-title">Change User Password of [ {{ $user->name }} ]</h3>
            	</div>
            	<!-- /.box-header -->
            	<!-- form start -->
            	{!! Form::open(['method' => 'PATCH', 'route' => ['users.change_pass', $user->id]]) !!}
	              	<div class="box-body">
		                <div class="form-group">
		                	{!! Form::label('password', 'New Password') !!}
		                	{!! Form::password('password', ['placeholder' => 'Enter New Password', 'class' => 'form-control']); !!}
		                </div>
		                <div class="form-group">
		                	{!! Form::label('password_confirmation', 'Confirm Password') !!}
		                	{!! Form::password('password_confirmation', ['placeholder' => 'Enter Confirm Password', 'class' => 'form-control']); !!}
		                </div>
	              	</div>
	              	<!-- /.box-body -->

	              	<div class="box-footer text-center">
		              	<button type="submit" class="btn bg-blue btn-flat ajax-submit">Save</button>
	              		<a href="{{ route('users.index') }}" class="btn bg-red btn-flat">Cancel</a>
	              	</div>
            	{!! Form::close() !!}
          	</div>
          	<!-- /.box -->
        </div>
        <!--/.col (left) -->
	</div>
@endsection
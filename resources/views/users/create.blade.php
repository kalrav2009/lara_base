@extends('layouts.app')

@section('title', 'Users')
@section('sub_title', 'Create User')

@section('content')
	<div class="row">
		<!-- left column -->
        <div class="col-md-12">
          	<!-- general form elements -->
          	<div class="box box-primary">
            	<div class="box-header with-border">
              		<h3 class="box-title">Create User</h3>
            	</div>
            	<!-- /.box-header -->
            	<!-- form start -->
            	{!! Form::open(['route' => ['users.store'], 'files' => true]) !!}
	              	<div class="box-body">
	              		<div class="row">
	              			<div class="col-md-6">
	              				<div class="form-group">
				                	{!! Form::label('name', 'Name', ['class' => 'required']) !!}
				                	{!! Form::text('name', null, ['placeholder' => 'Enter name', 'class' => 'form-control']); !!}
				                </div>
	              			</div>
	              			<div class="col-md-6">
	              				<div class="form-group">
				                	{!! Form::label('email', 'E-Mail Address', ['class' => 'required']) !!}
				                	{!! Form::email('email', null, ['placeholder' => 'Enter email', 'class' => 'form-control']); !!}
				                </div>
	              			</div>
	              		</div>
		                <div class="row">
		                	<div class="col-md-6">
		                		<div class="form-group">
				                	{!! Form::label('password', 'Password', ['class' => 'required']) !!}
				                	{!! Form::password('password', ['placeholder' => 'Enter Password', 'class' => 'form-control']); !!}
				                </div>
		                	</div>
		                	<div class="col-md-6">
		                		<div class="form-group">
				                	{!! Form::label('password_confirmation', 'Confirm Password', ['class' => 'required']) !!}
				                	{!! Form::password('password_confirmation', ['placeholder' => 'Enter Confirm Password', 'class' => 'form-control']); !!}
				                </div>
		                	</div>
		                </div>
		                <div class="row">
		                	<div class="col-md-6">
		                		<div class="form-group">
				                	{!! Form::label('role', 'Role', ['class' => 'required']) !!}
				                	{!! Form::select('role', $roles, null, ['placeholder' => 'Select User Role', 'class' => 'form-control']); !!}
				                </div>
		                	</div>
		                	<div class="col-md-6">
	              				<div class="form-group">
									{!! Form::label('status', 'Status', ['class' => 'required']) !!}
									<div class="clearfix"></div>
		                			<label for="status-active"><input id="status-active" type="radio" name="status" value="1" checked> Active</label>&nbsp;
                					<label for="status-inactive"><input id="status-inactive" type="radio" name="status" value="0"> InActive</label>
								</div>
	              			</div>
		                </div>
		                <div class="row">
		                	<div class="col-md-6">
		                		<div class="col-xs-3">
				                	<img src="{{ url('/assets/img').'/default-user.png' }}" class="img-responsive">
				                </div>
				                <div class="col-xs-9">
				                	<div class="form-group">
					                  	{!! Form::label('profile_image', 'Profile Image') !!}
					                  	{!! Form::file('profile_image') !!}
					                </div>
				                </div>
		                	</div>
		                </div>
	              	</div>
	              	<!-- /.box-body -->

	              	<div class="box-footer text-center">
		              	<button type="submit" class="btn bg-blue btn-flat ajax-submit">Save</button>
	              		<a href="{{ route('users.index') }}" class="btn bg-red btn-flat">Cancel</a>
	              	</div>
            	{!! Form::close() !!}
          	</div>
          	<!-- /.box -->
        </div>
        <!--/.col (left) -->
	</div>
@endsection

@push('scripts')
<!-- Page Js -->
<script src="{{asset('/assets/js/pages/user.js')}}"></script>
@endpush
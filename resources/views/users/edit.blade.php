@extends('layouts.app')

@section('title', 'Users')
@section('sub_title', 'Edit User')

@section('content')
	<div class="row">
		<!-- left column -->
        <div class="col-md-12">
          	<!-- general form elements -->
          	<div class="box box-primary">
            	<div class="box-header with-border">
              		<h3 class="box-title">Edit User</h3>
            	</div>
            	<!-- /.box-header -->
            	<!-- form start -->
            	{!! Form::open(['method' => 'PATCH', 'route' => ['users.update', $user->id], 'files' => true]) !!}
	              	<div class="box-body">
	              		<div class="row">
	              			<div class="col-md-6">
	              				<div class="form-group">
				                	{!! Form::label('name', 'Name') !!}
				                	{!! Form::text('name', $user->name, ['placeholder' => 'Enter name', 'class' => 'form-control']); !!}
				                </div>
	              			</div>
	              			<div class="col-md-6">
	              				<div class="form-group">
				                	{!! Form::label('email', 'E-Mail Address') !!}
				                	{!! Form::email('email', $user->email, ['placeholder' => 'Enter email', 'class' => 'form-control']); !!}
				                </div>
	              			</div>
	              		</div>

	              		<div class="row">
		                	<div class="col-md-6">
		                		<div class="form-group">
				                	{!! Form::label('role', 'Role') !!}
				                	{!! Form::select('role', $roles, (isset($user->roles[0]))?$user->roles[0]->id:null, ['placeholder' => 'Select User Role', 'class' => 'form-control']); !!}
				                </div>
		                	</div>
		                	<div class="col-md-6">
	              				<div class="form-group">
									{!! Form::label('status', 'Status') !!}
									<div class="clearfix"></div>
		                			<label for="status-active"><input id="status-active" type="radio" name="status" value="1" <?php echo ($user->status == 1) ? 'checked' : ''; ?>> Active</label>&nbsp;
                					<label for="status-inactive"><input id="status-inactive" type="radio" name="status" value="0" <?php echo ($user->status == 0) ? 'checked' : ''; ?>> InActive</label>
								</div>
	              			</div>
		                </div>
		                
		                <div class="row">
		                	<div class="col-md-6">
		                		<div class="col-xs-3">
				                	<img src="{{ (!empty($user->profile_image))?url($user->profile_image):url('/assets/img').'/default-user.png' }}" class="img-responsive">
				                </div>
				                <div class="col-xs-9">
				                	<div class="form-group">
					                  	{!! Form::label('profile_image', 'Profile Image') !!}
					                  	{!! Form::file('profile_image') !!}
					                </div>
				                </div>
		                	</div>
		                </div>
	              	</div>
	              	<!-- /.box-body -->

	              	<div class="box-footer text-center">
		              	<button type="submit" class="btn bg-blue btn-flat ajax-submit">Save</button>
	              		<a href="{{ route('users.index') }}" class="btn bg-red btn-flat">Cancel</a>
	              	</div>
            	{!! Form::close() !!}
          	</div>
          	<!-- /.box -->
        </div>
        <!--/.col (left) -->
	</div>
@endsection

@push('scripts')
<!-- Page Js -->
<script src="{{asset('/assets/js/pages/user.js')}}"></script>
@endpush

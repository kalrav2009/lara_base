@extends('layouts.app')

@section('title', 'User')
@section('sub_title', 'View User')

@section('content')
    <div class="row">
        <div class="col-md-3">
			<!-- Profile Image -->
			<div class="box box-primary">
				<div class="box-body box-profile">
					<img class="profile-user-img img-responsive img-circle" src="{{ (!empty($user->profile_image))?url($user->profile_image):url('/assets/img').'/default-user.png' }}" alt="User profile picture">
					<h3 class="profile-username text-center">{{ $user->name }}</h3>
					<p class="text-muted text-center">{{ $user->role }}</p>
					<ul class="list-group list-group-unbordered">
						<li class="list-group-item">
							<b>Name</b> <a class="pull-right">{{ $user->name }}</a>
						</li>
						<li class="list-group-item">
							<b>Email Id</b> <a class="pull-right">{{ $user->email }}</a>
						</li>
						@forelse($user->roles AS $role)
						<li class="list-group-item">
							<b>Role</b> <a class="pull-right">{{ $role->display_name }}</a>
						</li>
						@empty
						<li class="list-group-item">
							<b>Role</b> <a class="pull-right"> N/A </a>
						</li>
						@endforelse
						<li class="list-group-item">
							<b>Member Since</b> <a class="pull-right">{{ (new Carbon($user->created_at))->format('m/d/Y g:i A') }}</a>
						</li>
					</ul>
					<a href="{{ route('users.edit', $user->id) }}" class="btn bg-green btn-flat btn-block"><b>Edit</b></a>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@stop
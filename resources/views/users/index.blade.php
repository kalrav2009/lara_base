@extends('layouts.app')

@section('title', 'Users')
@section('sub_title', 'Users Listing')

@push('styles')
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('/assets/plugins/datatables/dataTables.bootstrap.css')}}">
@endpush

@section('content')
    <div class="row">
        <div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Users List</h3>
					<a href="{{ route('users.create') }}" class="btn bg-purple btn-flat pull-right"><i class="fa fa-plus"></i> Create New</a>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<table class="table table-bordered table-striped" id="users-table">
						<thead>
							<tr>
								<th>Id</th>
				                <th>Name</th>
				                <th>Email</th>
				                <th>Status</th>
				                <th width="125">Actions</th>
							</tr>
						</thead>
					</table>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@stop

@push('scripts')
<!-- DataTables -->
<script src="{{asset('/assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('/assets/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript">
	var dTable = '';
	jQuery(function() {
	    dTable = jQuery('#users-table').DataTable({
	        processing: true,
	        serverSide: true,
	        ajax: '{{ route('users.data') }}',
	        columns: [
	            { data: 'id', name: 'id' },
	            { data: 'name', name: 'name' },
	            { data: 'email', name: 'email' },
	            { data: 'status', name: 'status' },
	            { data: 'actions', name: 'actions', orderable: false, searchable: false, sClass : 'text-center' }
	        ]
	    });
	});

	function loadDataTable() {
	    dTable.ajax.reload( null, false );
	}
</script>
@endpush
@extends('layouts.app')

@section('title', 'Option')
@section('sub_title', 'Create Option')

@section('content')
	<div class="row">
		<!-- left column -->
        <div class="col-md-6">
          	<!-- general form elements -->
          	<div class="box box-primary">
            	<div class="box-header with-border">
              		<h3 class="box-title">Create Option</h3>
            	</div>
            	<!-- /.box-header -->
            	<!-- form start -->
            	{!! Form::open(['route' => ['options.store']]) !!}
	              	<div class="box-body">
		                <div class="form-group">
							<div class="form-group">
			                	{!! Form::label('type', 'Type') !!}
			                	{!! Form::select('type', Config::get('constants.options_categories'), null, ['placeholder' => 'Select Type', 'class' => 'form-control']); !!}
			                </div>
						</div>

		                <div class="form-group">
							{!! Form::label('title', 'Title') !!}
							{!! Form::text('title', null, ['placeholder' => 'Enter Title', 'class' => 'form-control']); !!}
						</div>

						<div class="form-group">
							{!! Form::label('status', 'Status') !!}
							<div class="clearfix"></div>
                			<label for="status-active"><input id="status-active" type="radio" name="status" value="1" checked> Active</label>&nbsp;
                			<label for="status-inactive"><input id="status-inactive" type="radio" name="status" value="0"> InActive</label>
						</div>						
	              	</div>
	              	<!-- /.box-body -->

	              	<div class="box-footer text-center">
		              	<button type="submit" class="btn bg-blue btn-flat ajax-submit">Save</button>
	              		<a href="{{ route('options.index') }}" class="btn bg-red btn-flat">Cancel</a>
	              	</div>
            	{!! Form::close() !!}
          	</div>
          	<!-- /.box -->
        </div>
        <!--/.col (left) -->
	</div>
@endsection
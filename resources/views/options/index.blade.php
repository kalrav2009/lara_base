@extends('layouts.app')

@section('title', 'Options')
@section('sub_title', 'Options Listing')

@push('styles')
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('/assets/plugins/datatables/dataTables.bootstrap.css')}}">
@endpush

@section('content')
    <div class="row">
        <div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Options List</h3>
					<a href="{{ route('options.create') }}" class="btn bg-purple btn-flat pull-right"><i class="fa fa-plus"></i> Create New</a>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<table class="table table-bordered table-striped" id="options-table">
						<thead>
							<tr>
								<th width="250">Type</th>
				                <th>Title</th>
				                <th width="50">Status</th>
				                <th width="85">Actions</th>
							</tr>
						</thead>
					</table>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@stop

@push('scripts')
<!-- DataTables -->
<script src="{{asset('/assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('/assets/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript">
	var dTable = '';
	jQuery(function() {
	    dTable = jQuery('#options-table').DataTable({
	        processing: true,
	        serverSide: true,
	        ajax: '{{ route('options.data') }}',
	        columns: [
	            { data: 'type', name: 'type' },
	            { data: 'title', name: 'title' },
	            { data: 'status', name: 'status' },
	            { data: 'actions', name: 'actions', orderable: false, searchable: false, sClass: 'text-center' }
	        ]
	    });
	});

	function loadDataTable() {
	    dTable.ajax.reload( null, false );
	}
</script>
@endpush
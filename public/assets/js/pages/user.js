jQuery(document).ready( function () {

	jQuery('#user_type').on('change', function () {
		var user_type = jQuery(this).val();
		if(user_type == 'office_user'){
			jQuery('.office_user_field').show();
		} else {
			jQuery('.office_user_field').hide();
			jQuery('#has_pf').iCheck('uncheck');
			jQuery('#has_esi').iCheck('uncheck');
			jQuery('.has_pf_field').hide();
			jQuery('.has_esi_field').hide();
			jQuery('#pf_percent').val('');
			jQuery('#esi_percent').val('');
			jQuery('#role').val('');
			jQuery('#designation').val('');
			jQuery('#start_date').val('');
			jQuery('#end_date').val('');
		}
	});

	jQuery('#has_pf').on('ifChecked', function () {
		jQuery('.has_pf_field').show();
	});
	jQuery('#has_pf').on('ifUnchecked', function () {
		jQuery('.has_pf_field').hide();
		jQuery('#pf_percent').val('');
	});

	jQuery('#has_esi').on('ifChecked', function () {
		jQuery('.has_esi_field').show();
	});
	jQuery('#has_esi').on('ifUnchecked', function () {
		jQuery('.has_esi_field').hide();
		jQuery('#esi_percent').val('');
	});

});
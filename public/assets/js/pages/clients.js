jQuery(document).ready( function () {
	jQuery('#contact_details_append').on('click', '.add-row', function (e) {
		var parent = jQuery(this).parents('.detail-row:first');
		var row_id = parent.attr('data-row-id');
		var template = jQuery('#detail-row-template').html();
		jQuery(this).hide();
		parent.find('.remove-row').show();
		template = template.replace(/%i%/gi, parseInt(row_id)+1);
		jQuery('#contact_details_append').append(template);
	});
	jQuery('#contact_details_append').on('click', '.remove-row', function (e) {
		jQuery(this).parents('.detail-row:first').remove();
	});

	jQuery('#has_pf').on('ifChecked', function () {
		jQuery('.has_pf_field').show();
	});
	jQuery('#has_pf').on('ifUnchecked', function () {
		jQuery('.has_pf_field').hide();
		jQuery('#pf_percentage').val('');
	});

	jQuery('#has_esi').on('ifChecked', function () {
		jQuery('.has_esi_field').show();
	});
	jQuery('#has_esi').on('ifUnchecked', function () {
		jQuery('.has_esi_field').hide();
		jQuery('#esi_percentage').val('');
	});

	jQuery('#has_wcp').on('ifChecked', function () {
		jQuery('.has_wcp_field').show();
	});
	jQuery('#has_wcp').on('ifUnchecked', function () {
		jQuery('.has_wcp_field').hide();
		jQuery('#wcp_percentage').val('');
	});

});
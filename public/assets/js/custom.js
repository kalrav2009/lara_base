$(function() {
	$.ajaxSetup({
		headers: {
			'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
		}
	});
});

jQuery(document).ready(function () {
	//universal ajax submit function
	jQuery(document).on('click', '.ajax-submit', function (e) {
		e.preventDefault();
		e.stopPropagation();
		var submit_btn = jQuery(this);
		var btn_name = jQuery(this).html();
		var new_btn_name = 'Loading...'
		var form = jQuery(this).parents('form:first');
		var method = jQuery(form).attr('method');
		var url = jQuery(form).attr('action');
		
		jQuery(form).ajaxSubmit({
			type: method,
			url: url,
			dataType : 'JSON',
			beforeSend : function() {
				jQuery(submit_btn).html(new_btn_name);
				jQuery(submit_btn).attr('disabled', true);
				jQuery(form).find('.form-group').removeClass('has-error');
				jQuery(form).find('.help-block').remove();
			},
			success : function(data) {
				if(data.response == 1){
					swal({   
						title: "Done",   
						text: data.msg,   
						type: "success", 
						closeOnConfirm: false 
					}, 
					function(){   
						if(data.redirect){
							location.replace(data.redirect);
						} else {
							location.reload();	
						}
					});
				} else if (data.response == 2){
					sweetAlert("Oops...", data.msg, "error");
				}
			},
			error : function (data) {
			    jQuery.each(data.responseJSON, function (key, index) {
			    	if(~key.indexOf(".")){
				        key = key.replace(/\./gi, '-');
				        jQuery('#'+key).closest('.form-group').addClass('has-error').append('<span class="help-block">'+index[0]+'</span>');
			    	} else {
			    		var input = jQuery(form).find('[name="'+key+'"]');
				        input.closest('.form-group').addClass('has-error').append('<span class="help-block">'+index[0]+'</span>');
			    	}
			    });
			},
			complete : function(){
				jQuery(submit_btn).html(btn_name);
				jQuery(submit_btn).attr('disabled', false);
			}
		});
	});

	//universal delete function
	jQuery(document).on('click', '.btn-delete', function (e) {
		e.preventDefault();
		var url = jQuery(this).attr('href');
		swal({   
			title: "Delete Data",   
			text: "Are You Sure You Want To Delete Data",   
			type: "warning",   
			showCancelButton: true,   
			closeOnConfirm: false,   
			showLoaderOnConfirm: true, 
		}, function(){   
			jQuery.ajax({
				type: 'DELETE',
                data: {_method: 'DELETE'},
				dataType : 'JSON',
				url: url,
				success : function(data) {
					if(data.response == 1){
						swal({
							title: "Done",   
							text: data.msg,   
							type: "success", 
							closeOnConfirm: true
						}, 
						function(){
							if (typeof data.redirect != 'undefined'){
								location.replace(data.redirect);
							} else {
								loadDataTable();	
							}
						});
					} else if (data.response == 2){
						swal("Oops...", data.msg, "error");
					}
				},
			});
		});
	});

	//modal ajax submit function
	jQuery(document).on('click', '.modal-submit', function (e) {
		e.preventDefault();
		e.stopPropagation();
		var submit_btn = jQuery(this);
		var btn_name = jQuery(this).html();
		var new_btn_name = 'Loading...'
		var form = jQuery(this).parents('form:first');
		var method = jQuery(form).attr('method');
		var url = jQuery(form).attr('action');

		jQuery(form).ajaxSubmit({
			type: method,
			url: url,
			dataType : 'JSON',
			beforeSend : function() {
				jQuery(submit_btn).html(new_btn_name);
				jQuery(submit_btn).attr('disabled', true);
				jQuery(form).find('.form-group').removeClass('has-error');
				jQuery(form).find('.help-block').remove();
			},
			success : function(data) {
				jQuery('#empty_modal').modal('hide');
				if(data.response == 1){
					swal({
						title: "Done",   
						text: data.msg,   
						type: "success", 
						closeOnConfirm: true
					}, 
					function(){
						loadDataTable();
					});
				} else if (data.response == 2){
					sweetAlert("Oops...", data.msg, "error");
				}
			},
			error : function (data) {
			    jQuery.each(data.responseJSON, function (key, index) {
			    	if(~key.indexOf(".")){
				        key = key.replace(/\./gi, '-');
				        jQuery('#'+key).closest('.form-group').addClass('has-error').append('<span class="help-block">'+index[0]+'</span>');
			    	} else {
			    		var input = jQuery(form).find('[name="'+key+'"]');
				        input.closest('.form-group').addClass('has-error').append('<span class="help-block">'+index[0]+'</span>');
			    	}
			    });
			},
			complete : function(){
				jQuery(submit_btn).html(btn_name);
				jQuery(submit_btn).attr('disabled', false);
			}
		});
	});

	var current_url= window.location.href;
	jQuery('.sidebar').find('a').each(function(){
		if(current_url == jQuery(this).attr('href')){
			jQuery(this).parents('li').addClass('active');
		}
	});
	
	//Date picker
    $('.datepicker').datepicker({
    	autoclose: true
    });
    
    //Timepicker
    $(".timepicker").timepicker({
      	showInputs: false,
    });

    //Initialize Select2 Elements
    jQuery(".select2").select2();

    /*** I CHECK JQUERY **/ 
    	jQuery('input').iCheck({
      		checkboxClass: 'icheckbox_square-blue',
      		radioClass: 'iradio_square-blue',
      		increaseArea: '20%'
	    });
    /* ********************** */

    /** Input Mask **/
    	jQuery(".mask-percentage").inputmask('decimal', {
    		radixPoint				: ".",
    		autoGroup				: true,
    		clearMaskOnLostFocus	: false,
    		digits 					: 2,
    	});
    /* *********************** */

    //tooltips
    jQuery('[data-toggle="tooltip"]').tooltip();

});
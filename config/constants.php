<?php
	return [
		'customPermissionGroups' => [
			'CustomPermissions' => 'CustomPermissions',
		],
		'options_categories' => [
			'salary_fields'  => 'Salary Fields', 
			'client_category'=> 'Client Category ', 
			'quotation_zone' => 'Quotation Zone',
			'quotation_type' => 'Quotation Type',
			'quotation_sub_type'=>'Quotation SubType',
			'quotation_skill'=>'Quotation Skill',
			'quotation_skill_sub'=>'Quotation Sub Skill'
		],
	]
?>
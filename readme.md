# After Git Clone Do Following Steps #
* Run : composer update
* Create empty database
* Create .env File and Set db Credentials (set CACHE_DRIVER=array)
* Run : php artisan key:generate
* Run : php artisan migrate:refresh --seed
* Run : php artisan serve
* Go to http://127.0.0.1:8000/admin/login
* Admin User : email - admin@yopmail.com , pass - 123456
* Only UI Level Restrictions are implemented follow zizaco/Entrust Guide to restrict route or controllers (pending implementation)
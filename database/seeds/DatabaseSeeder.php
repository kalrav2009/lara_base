<?php

use Illuminate\Database\Seeder;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('permissions')->insert([
            [
            	'name' => 'create_user',
                'display_name' => 'Create User',
                'permission_group' => 'User'
            ],[
            	'name' => 'list_user',
                'display_name' => 'List User',
                'permission_group' => 'User'
            ],[
            	'name' => 'destroy_user',
                'display_name' => 'Destroy User',
                'permission_group' => 'User'
            ],[
            	'name' => 'edit_user',
                'display_name' => 'Edit User',
                'permission_group' => 'User'
            ],[
            	'name' => 'show_user',
                'display_name' => 'Show User',
                'permission_group' => 'User'
            ],[
            	'name' => 'create_role',
                'display_name' => 'Create Role',
                'permission_group' => 'Role'
            ],[
            	'name' => 'list_role',
                'display_name' => 'List Role',
                'permission_group' => 'Role'
            ],[
            	'name' => 'destroy_role',
                'display_name' => 'Destroy Role',
                'permission_group' => 'Role'
            ],[
            	'name' => 'edit_role',
                'display_name' => 'Edit Role',
                'permission_group' => 'Role'
            ],[
            	'name' => 'create_permission',
                'display_name' => 'Create Permission',
                'permission_group' => 'Permission'
            ],[
            	'name' => 'list_permission',
                'display_name' => 'List Permission',
                'permission_group' => 'Permission'
            ],[
            	'name' => 'destroy_permission',
                'display_name' => 'Destroy Permission',
                'permission_group' => 'Permission'
            ],[
            	'name' => 'edit_permission',
                'display_name' => 'Edit Permission',
                'permission_group' => 'Permission'
            ],[
                'name' => 'manage_roles_and_permissions',
                'display_name' => 'Manage Roles And Permissions',
                'permission_group' => 'CustomPermissions'
            ]
        ]);

    	$role = DB::table('roles')->insertGetId([
            'name' => 'admin',
            'display_name' => 'Admin',
            'description' => 'Administrator of system',
        ]);

        $user = User::create([
            'name' => 'Administrator',
            'email' => 'admin@yopmail.com',
            'password' => bcrypt('123456')
        ]);

        $user->attachRole($role);
    
        DB::table('roles')->insert([
            [
                'name' => 'user',
                'display_name' => 'Front User',
                'description' => 'Front User'
            ]
        ]);
    }
}
